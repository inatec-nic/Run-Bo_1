Categoría: Avanzado
Temática: Libre
Nombre del Equipo: Crafters
Nombre del Proyecto: Run.Bo
Descripción del Proyecto:
Descripción General:

Run.Bo es un proyecto que tiene como objetivo, notificar o alertar a las personas que hacen uso del transporte público y selectivo, cuando se aproxima a su ubicación.

A Través de Run.Bo, podrás saber a qué distancia se encuentra tu transporte puede ser en kilómetros, metros e incluso en tiempo, permitiéndole al usuario una optimización de su tiempo y menos probabilidad de perder tu transporte.


Mecánica:

Los conductores de los medios de transporte podrán descargar la aplicación, la cual aprovechará el teléfono inteligente de uso personal o empresarial.

El Conductor:
Una vez descargada la app por el conductor, se realizará el proceso de registro, en el cual se solicitaran datos como: si es Conductor o Cliente  . En caso de ser transporte público este podrá indicar su punto de partida y su punto de fin, luego durante el uso, el app segun la posicion de los clientes (pasajeros) indicará si se encuentra cerca o lejos y en cuánto tiempo estará en su ubicación.

En caso de ser transporte selectivo los Clientes podrán saber cuál está más cerca he incluso solicitar el servicio al mejor estilo de empresas com Waze y Uber.

El app tendrá el potencial para calcular un aproximado de personas que usan el transporte, facilitando información al dueño del transporte como: número de usuarios por dia, semana y mes, aproximado de ingresos, zona de carga más usada, horario de mayor uso del medio entre otros.


Que no te deje el bus Run.Bo es tu mejor opcion...

Firebase Google Location API API de Google Maps
GeoFire
Cuchillo de mantequilla
Madera
Firebase Auth
Base de datos de Firebase
Android Studio